import datetime
import re

class DateParser:


    def ParseGoogleDate(self,day_raw,hour_raw):
        month = self.ChangeMonthInWordsTo2Digits(day_raw)
        day = self.ExtractDays(day_raw)
        if month == 12 and datetime.datetime.now().month == 1:
            year = str(datetime.datetime.now().year) - 1
        else:
            year = str(datetime.datetime.now().year)
        time = hour_raw
        date_time = year + '-' + month + '-' + day  + 'T' + time + ':00'
        return date_time

    def ChangeMonthInWordsTo2Digits(self, day_raw):
        list_months = ['sty', 'lut', 'mar', 'kwie', 'maj', 'czerw', 'lip', 'sierp', 'wrze', 'paź', 'list', 'gru']
        for count, elem in enumerate(list_months, 1):
            if elem in day_raw:
                month = str(count)
        if len(month) == 1:
            month = '0' + month
        return month
        
    def ExtractDays(self,day_raw):
        day = re.findall(r'\d+', day_raw)[-1]
        if len(day) == 1:
            day = '0' + day
        return day

    def ParseDatabaseDate(self,day_raw,hour_raw):
        month = self.ChangeMonthInWordsTo2Digits(day_raw)
        day = self.ExtractDays(day_raw)
        if month == 12 and datetime.datetime.now().month == 1:
            year = str(datetime.datetime.now().year) - 1
        else:
            year = str(datetime.datetime.now().year)
        time = hour_raw
        date_time = year + '-' + month + '-' + day + ' ' + time + ':00'
        return date_time

    def stripStringBeforeNumber(self, text):
        for i in range(len(text)):
            if text[i].isdecimal():
                return text[i:]
                break  