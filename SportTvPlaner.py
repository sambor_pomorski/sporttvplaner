from __future__ import print_function
import tkinter as tk
import tkinter.ttk as ttk
import sqlite3
import datetime
import threading
from tkinter.colorchooser import *            
import GoogleCalendarAPI
import DatabaseHandler
import LibraryHelper
import webbrowser 
from tkinter.filedialog import askopenfilename #tkFileDialog #tkinter.filedialog
from tkinter import messagebox


class Application:
    #======================
    # create variable for time, load gifs needed for gui, create gui, connect with database, insert columns and headings to treeview 
    #======================
    def __init__(self, root):
        self.now = datetime.datetime.now()
        self.root = root
        with open("settingsFiles/username.txt", "r") as f:
            self.user = (f.read())
        with open("settingsFiles/kanaly.txt", "r") as g:
            self.channels_raw = (g.read())
        self.channels = self.channels_raw.split(",")  
        self.load_gifs()
        self.create_gui()
        self.DataHandler = DatabaseHandler.DatabaseHandler()
        self.select_records()

    #======================
    # load icons
    #======================
    def load_gifs(self):
        self.refresh = tk.PhotoImage(file = 'icons/refresh1.gif') #
        self.plus = tk.PhotoImage(file = 'icons/plus.gif')
        self.select = tk.PhotoImage(file = 'icons/wybierz.gif')
        self.lupa = tk.PhotoImage(file = 'icons/lupaa.gif')
        self.live = tk.PhotoImage(file = 'icons/live.gif')
        self.settings = tk.PhotoImage(file = 'icons/settings.gif')
        self.calendar = tk.PhotoImage(file = 'icons/calendar.gif')

    #======================
    # create GUI
    #======================
    def create_gui(self):
        #======================
        # create tab control
        #======================
        self.tab_control = ttk.Notebook(root)          # Create Tab Control
        self.tab_control.pack()
        self.tab = tk.Frame(root, width = 868, height = 576)
        self.tab2 = tk.Frame(root, width=868, height=576)
        #======================
        # create frame for buttons, and buttons 
        #======================
        self.frame_up = tk.Frame(self.tab, padx = 2, pady = 7)
        self.frame_up.pack(anchor = tk.W)
        self.button_add = tk.Button(self.frame_up, text = "Dodaj", image = self.plus, command = self.window_add_sport_event)
        self.button_add.pack(side = tk.LEFT,padx = 2, pady = 2)
        self.button_channel = tk.Button(self.frame_up, text = "Wybierz swoje kanały", image = self.select, command = self.window_choose_channels)
        self.button_channel.pack(side = tk.LEFT,padx = 2, pady = 2)
        self.button_refresh = tk.Button(self.frame_up, text = "Odśwież", image = self.refresh, command = self.update_thread)
        self.button_refresh.pack(side = tk.LEFT,padx = 2, pady = 2)
        self.button_settings = tk.Button(self.frame_up, text = "Ustawienia", image = self.settings, command = self.window_settings)
        self.button_settings.pack(side = tk.LEFT, padx = 2, pady = 2)
        self.button_webbrowser = tk.Button(self.frame_up, text = "Otwórz kalendarz", image = self.calendar, command = self.open_webbrowser)
        self.button_webbrowser.pack(side = tk.LEFT, padx = 1)
        #======================
        # create frame for select_records and elements
        #======================
        self.frame_low = tk.Frame(self.tab, height = 1000,padx=6, pady=2) 
        self.frame_low.pack(fill =tk.X)
        self.var_select_records = tk.StringVar()
        self.entry_select_records = tk.Entry(self.frame_low, textvariable = self.var_select_records)
        self.entry_select_records.pack(anchor = tk.W, side = tk.LEFT)
        self.entry_select_records.bind("<Return>", self.select_records)
        self.button_select_records = tk.Button(self.frame_low, image = self.lupa, command = self.select_records)
        self.button_select_records.pack(side = tk.LEFT, padx = 1)
        self.var_live = tk.IntVar()
        self.chekckbutton_live = tk.Checkbutton(self.frame_low, text = 'Live', variable = self.var_live, command = self.select_records)
        self.chekckbutton_live.pack(side = tk.LEFT, padx = 25)
        #======================
        # create treeview and scrollbar
        #======================
        self.tree = ttk.Treeview(self.tab)
        self.scrollbar = ttk.Scrollbar(self.tab, orient=tk.VERTICAL, command=self.tree.yview)
        self.tree.configure(xscrollcommand=self.scrollbar.set,yscrollcommand=self.scrollbar.set)
        self.scrollbar.pack(side = 'right', fill='y')
        self.tree.pack(expand=True, fill='both')
        #======================
        # create progressbar and read when last loaded from file
        #======================
        self.frame_down = tk.Frame(self.root, height = 10,padx=6, pady=3) 
        self.frame_down.pack(fill =tk.X)
        self.progressbar = ttk.Progressbar(self.frame_down, orient = 'horizontal', length = 100, mode = 'determinate')
        self.progressbar.pack(side = tk.LEFT)
        self.var_lastupdate = tk.StringVar()
        self.file_last_update = open("settingsFiles/last_update.txt", "r")
        try:
            self.var_lastupdate.set(self.file_last_update.read())
        except:
            pass
        self.label = tk.Label(self.frame_down, textvariable = self.var_lastupdate).pack(side = tk.LEFT)
        #======================
        # make treeview
        #======================     
        #tab_control.pack(fill='y', expand='yes')  # Pack to make visible
        tabsy = self.tab_control.add(self.tab, text='Wydarzenia')      # Add the tab
        self.tab_control.add(self.tab2, text='')      # Add the second tab
        self.tab_control.tab(1, state="disabled")
        self.tree["columns"]=("Dzień","Godzina", "Tytuł", "Stacja") #tworzymy kolumy
        self.tree.column("#0", width=50)  #sprawiamy by pierwsza kolumna była wąska
        self.tree.column("Dzień", width=100)
        self.tree.column("Godzina", width=65)
        self.tree.column("Stacja", width=125)
        self.tree.column("Tytuł", width=600)
        self.tree.heading("Dzień", text="Dzień")     #tworzymy nagłówki
        self.tree.heading("Godzina", text="Godzina")
        self.tree.heading("Stacja", text="Stacja")
        self.tree.heading("Tytuł", text="Tytuł")

    #======================
    # new window to add event
    #======================
    def window_add_sport_event(self):
        try:
            self.item = self.tree.focus()
            self.choosen_item = (self.tree.item(self.item))
            self.event_description = (self.choosen_item.get('values'))
            self.LibraryHelper = LibraryHelper.DateParser()
            self.event_date = self.LibraryHelper.ParseGoogleDate(self.event_description[0], self.event_description[1]) #, 'google')        
            self.event_title = self.event_description[2]
            self.event_channel = self.event_description[3]
            self.new_window = tk.Toplevel(self.root)
            self.new_window.resizable(width=False, height=False)
            self.new_window.title("Dodaj wydarzenie")
            ttk.Label(self.new_window, text = 'Czy chcesz dodać wydarzenie?', font = "Helvetica 12 bold").pack(padx = 10)
            ttk.Label(self.new_window, text = self.event_title, font = "Helvetica 11 bold", wraplength = 350, justify = 'center', foreground = 'blue').pack(padx = 10, pady = 5)
            ttk.Label(self.new_window, text = self.event_channel , font = "Helvetica 11 bold").pack()
            tk.Button(self.new_window, text = 'OK', command = self.invokeGoogleCalendarAPI, width = 9, font = "Helvetica 12 bold").pack(pady = 10)
        except Exception as e:
            messagebox.showinfo("Informacja", "Zaznacz wydarzenie sportowe z listy")

    #======================
    # create grid with checkbutton to choose station, list of checkbuttons and intvars
    #======================
    def window_choose_channels(self):
        self.new_window = tk.Toplevel(self.root)
        self.new_window.resizable(width=False, height=False)
        self.new_window.title("Wybierz kanały")
        self.var = []
        self.cb = []
        self.list_channels = (self.DataHandler.c.execute("SELECT DISTINCT Stacja FROM sportwtv ORDER BY Stacja"))
        COLUMN = 0
        self.list_channels=[elt[COLUMN] for elt in self.list_channels] #['nSport+', 'Eurosport 1', 'Eurosport 2', 'Polsat Sport News', 'Polsat Sport', 'Polsat Sport Extra', 'CANAL+ Sport', 'TVP Sport', 'TVP 2', 'Polsat', 'TVN', 'TVP 1', 'CANAL+ Family', 'CANAL+', 'CANAL+ 1', 'TV 4', 'TVN 7']
     
        grid_divider = len(self.list_channels)//3 + 1
        for w in range (len(self.list_channels)):
            self.var.append(tk.IntVar())
            self.cb.append(tk.Checkbutton(self.new_window, text = self.list_channels[w], variable = self.var[w]))
            if self.cb[w]['text'] in self.channels:
                self.cb[w].select()
            if w >= 2*grid_divider :
                row = w % grid_divider
                column = 2
            elif w >=  grid_divider:
                row = w % grid_divider
                column = 1
            else:
                row = w
                column = 0
            self.cb[w].grid(row = row, column = column, sticky = tk.W)
        self.button_save = tk.Button(self.new_window, text = "Zapisz", command = self.update_channels_selected)
        self.button_save.grid(row = grid_divider + 1, columnspan = 3, pady = 6)

    #======================
    # new window to change settings
    #======================
    def window_settings(self):
        self.new_window = tk.Toplevel(self.root)
        self.new_window.resizable(width=False, height=False)
        self.new_window.title("Ustawienia")
        tabControl = ttk.Notebook(self.new_window)
        tab1 = ttk.Frame(tabControl)
        tabControl.add(tab1, text = 'Użytkownicy')
        tabControl.pack(expand = 1, fill = "both")
        labelsFrame = ttk.LabelFrame(tab1, text="Twoje konto Google do \nsychnronizacji z kalendarzem") # 1
        labelsFrame.grid(column=0, row=0, padx = 10, pady = 20)
        # Place labels into the container element # 2
        self.var_username = tk.StringVar()
        self.entry_username = ttk.Entry(labelsFrame, textvariable = self.var_username, width = 30)
        self.file_username = open("settingsFiles/username.txt", "r")
        username = (self.file_username.read())
        self.var_username.set(username)
        self.entry_username.grid(row=1, column=0, padx=10, pady=10)
        self.button_save_settings = tk.Button(tab1, text = "Zapisz", command = self.save_user, padx = 15, pady = 5) #, command = self.update_channels_selected)
        self.button_save_settings.grid(row = 2, columnspan = 1, pady = 1)
        tab2 = ttk.Frame(tabControl)
        tabControl.add(tab2, text = 'Przeglądarka')
        self.labelsFrame3 = ttk.LabelFrame(tab2, text='Wybierz przeglądarkę na której \n jesteś zalogowany do konta Google') # 1
        self.labelsFrame3.grid(column=0, row=0, pady = 10)
        self.var_browser = tk.IntVar()
        tk.Radiobutton(self.labelsFrame3, 
                      text="Przeglądarka domyślna",
                      padx = 20, 
                      pady = 5,
                      variable=self.var_browser, 
                      command = self.select_default_browser,
                      value=1).pack(anchor=tk.W)
        tk.Radiobutton(self.labelsFrame3, 
                      text="Wybierz manualnie przeglądarkę",
                      padx = 20, 
                      pady = 5,
                      variable=self.var_browser, 
                      command = self.select_other_browser,
                      value=2).pack(anchor=tk.W)
            
        f = open("settingsFiles/browser_path.txt", "r")
        browser_selection = f.read() 
        browser_text = "Wybrana przeglądarka: \n" + browser_selection
        tk.Label(self.labelsFrame3, text=browser_text, pady = 10).pack()

    def save_user(self):
        f = open("settingsFiles/username.txt", "w")
        f.write(self.var_username.get())
        self.user = self.var_username.get()
        f.close()
        self.new_window.destroy()

    def select_default_browser(self):
        f = open("settingsFiles/browser_path.txt", "w")
        f.write('default')
        f.close()
        self.new_window.destroy()
        #tk.Label(self.labelsFrame3, text='default', pady = 30).pack()


    def select_other_browser(self):
        self.other_browser_path = askopenfilename()
        f = open("settingsFiles/browser_path.txt", "w")
        f.write(self.other_browser_path)        
        if not self.other_browser_path:
            f.write('default')
        f.close()
        self.new_window.destroy()

       
    #======================
    # open web browser
    #======================
    def open_webbrowser(self):
        new = 2 # open in a new tab, if possible
        url = "https://calendar.google.com/calendar/"
        f = open("settingsFiles/browser_path.txt", "r")
        browser_selection = f.read()
        if browser_selection == 'default':
            webbrowser.open_new_tab(url)
        else:
            path = (browser_selection.replace("/" , "//"))
            webbrowser.register('otherbrowser', None, webbrowser.BackgroundBrowser(path),1)
            webbrowser.get('otherbrowser').open(url,new=new)



    def update_channels_selected(self):
        self.channels = []
        for w in range(len(self.var)):
            if self.var[w].get() == 1:
                self.channels.append(self.cb[w]['text'])
        placeholder= '?'
        self.placeholders= ', '.join(([placeholder]*len(self.channels)))
        f = open("settingsFiles/kanaly.txt", "w")
        channels = ','.join(map(str, self.channels)) 
        f.write(channels)
        f.close()
        self.select_records()
        self.new_window.destroy()


    def update_database(self):
        pages = 120
        self.DataUpdater = DatabaseHandler.UpdateDatabase(pages, self.progressbar,self.var_lastupdate, self.root)
        self.refresh_last_update_time()
        select = self.select_records()

           
    def select_records(self, s = None):
        placeholder = '?'
        self.placeholders= ', '.join(([placeholder]*len(self.channels)))
        entry_content = str(self.entry_select_records.get()) #TO CO JEST W OKNIE TEXT
        self.channels.insert(0, '%' + entry_content + '%')
        if self.var_live.get() == 1:
            data = self.DataHandler.c.execute("SELECT * FROM sportwtv WHERE NaZ = '1' AND Tytuł LIKE ? AND Stacja IN (%s) AND DataF > datetime('now', 'localtime', '-2 hours')"  %  self.placeholders , self.channels)           
        else:
            data = self.DataHandler.c.execute("SELECT * FROM sportwtv WHERE Tytuł LIKE ? AND Stacja IN (%s) AND DataF > datetime('now','localtime','-2 hours')" %  self.placeholders , self.channels)
        self._send_database_to_GUI(data)
        del self.channels[0]


    def _send_database_to_GUI(self, data):
        self.tree.delete(*self.tree.get_children())
        events_list = []
        columns = self.DataHandler.c.description
        for value in data: #przetarzamy kazdy wiersz z bazy
            tmp = {}
            for (index,column) in enumerate(value):
                tmp[columns[index][0]] = column #tmp[godzina/tytul/stacja[0]] ->zawsze zero, bo obecna, przyporzadkowujemy kolumnie wartosc do slownika
            events_list.append(tmp) #zalaczamy kazda komorke i dodajemy te 4 wartosci do wiersza i jednej wartosci listy
        self.tree.tag_configure('live_tag',image = self.live)
        counter = 0
        for w in events_list:
            counter +=1
            tmp = self.tree.insert('', 'end', iid = counter, values=(w['Dzień'],w['Godzina'],w['Tytuł'],w['Stacja'],w['NaZ']))#, image = self.live)
            if w['NaZ'] == 1:
                self.tree.item(counter, tags = 'live_tag')

    def update_thread(self):
        download_thread = threading.Thread(target = self.update_database)
        download_thread.start()

    def refresh_last_update_time(self):
        self.file_last_update = open("settingsFiles/last_update.txt", "w")
        timetxt = 'Ostatni raz zaktualizowano: ' + str(self.DataUpdater.loaded_date)
        self.var_lastupdate.set(timetxt)
        self.file_last_update.write(timetxt)
        self.file_last_update.close()

        
    def invokeGoogleCalendarAPI(self):
        GoogleAPI = GoogleCalendarAPI.GoogleCalendarAPI(self.event_title, self.event_channel, self.event_date, self.user)
        app.new_window.destroy()


       
root = tk.Tk()
root.resizable(0,0) 

root.title("Planner wydarzeń sportowych w TV")
app = Application(root)
root.mainloop()                          # Start GUI
