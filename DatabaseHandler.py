
import requests, bs4
import subprocess
import time
import datetime
import os
import sqlite3
import LibraryHelper
import EVENT_DATA

class DatabaseHandler:
    def __init__(self):
        self.conn = sqlite3.connect('sportwtv.db',check_same_thread=False) #tworzymy obiekt 'connection' ktory reprezentuje baze danych
        self.c = self.conn.cursor() #tworzymy kursor obiektu
     

class UpdateDatabase:

    def __init__(self, pages, progressbar, var_lastupdate, root):
        self.root = root
        self.DatabaseHandler = DatabaseHandler()
        self.LibraryHelper = LibraryHelper.DateParser()
        self.table = []
        self.teleman_soup = []
        self.pages = pages
        self.progressbar = progressbar
        self.var_lastupdate = var_lastupdate
        self.scrape_website()
        self.create_soup_table()
        self.create_objects()
        self.send_objects_to_database()
        self.loaded_date = time.strftime("%H:%M, %d-%m-%Y")
        #self.SelectDistinctTvStations()

    def scrape_website(self):
        self.var_lastupdate.set("Pobieram strony...")
        self.progressbar['maximum'] = self.pages
        for w in range(self.pages): #POBIERAMY INFO, DODAJEMY DO TABELI teleman_soup
            self.progressbar['value'] = w
            time.sleep(0.02)
            self.root.update_idletasks()
            print(w)               
            url = 'http://www.teleman.pl/sport?page=' + str(w+1) + '&stations=all'
            res = requests.get(url)
            try:
                res.raise_for_status()
            except Exception as exc:
                print('There was a problem: %s' % (exc))
                break          
            self.teleman_soup.append(bs4.BeautifulSoup(res.text, "html.parser" ))
            self.progressbar['value'] += 1
        self.root.after(100, self.zero_progressbar )

    def zero_progressbar(self):
        self.progressbar['value'] = 0
        
    def create_soup_table(self):
        for w in range(len(self.teleman_soup)):   #POBIERAMY TREŚCI KOMÓREK TABELI I WRZUCAMY DO LISTY TABLE
            self.table.append(self.teleman_soup[w].find('table', {'id': 'sports-listing'}))

    def create_objects(self):
        self.objects = []
        for w in range(len(self.table)): #get each page data
            counter = 0
            for row in self.table[w].findAll('tr'): #get each row from page
                if counter != 0:
                    sport_event = EVENT_DATA.EVENT_DATA()
                    temp_list = (row.text).split('\n')
                    sport_event.dateraw = temp_list[1]
                    sport_event.hour = temp_list[2]
                    sport_event.channel = temp_list[3]
                    sport_event.title = temp_list[4] + " " +  temp_list[5]             
                    sport_event.datecleaned = self.LibraryHelper.stripStringBeforeNumber(sport_event.dateraw)
                    sport_event.datetime = self.LibraryHelper.ParseDatabaseDate(sport_event.dateraw, sport_event.hour)
                    if row.select('.live'):
                        sport_event.isLive = 1
                    self.objects.append(sport_event)
                    counter +=1
                else:
                    counter +=1

    def send_objects_to_database(self):
        self.DatabaseHandler.c.execute('DROP TABLE IF EXISTS sportwtv;') #bez tego error, że mamy tabelę
        self.DatabaseHandler.c.execute('''CREATE TABLE sportwtv
                 (id INTEGER PRIMARY KEY, 
                 Dzień varchar(100), 
                 Godzina varchar(50), 
                 Stacja varchar(100), 
                 Tytuł varchar(255), 
                 DataF datetime, 
                 NaZ int);
                 ''')

        for w in range(len(self.objects)):
            try:
                self.DatabaseHandler.c.execute('''INSERT INTO sportwtv (Dzień, Godzina, Stacja, Tytuł, DataF, NaZ) values ('%s','%s', '%s','%s', '%s', '%s')'''% \
                              (self.objects[w].datecleaned,self.objects[w].hour,self.objects[w].channel,self.objects[w].title,self.objects[w].datetime,self.objects[w].isLive))#,self.listaLive[w]))
            except Exception as exc:
                print('skorzystano z except nr1. There was a problem: %s' % (exc))   #day, hour, channel, title, datetime, isLive
                self.DatabaseHandler.c.execute('''INSERT INTO sportwtv (Dzień, Godzina, Stacja, Tytuł, DataF, NaZ) values ("%s","%s", "%s","%s", "%s", "%s")'''% \
                              (self.objects[w].datecleaned,self.objects[w].hour,self.objects[w].channel,self.objects[w].title,self.objects[w].datetime,self.objects[w].isLive))#,self.listaLive[w]))
        self.DatabaseHandler.conn.commit()


